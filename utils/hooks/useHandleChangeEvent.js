import { useState } from "react";

export const useHandleChangeEvent = (initialValue) => {
  const [getter, setter] = useState(initialValue);
  const handleChange = (event) => {
    setter(event.target.value);
  };
  return [getter, handleChange];
};
