import Document, { Html, Head, Main, NextScript } from "next/document";
import CookieConsent from "react-cookie-consent";
import { ServerStyleSheets } from "@material-ui/core/styles";
import { ServerStyleSheet } from "styled-components";
import React from "react";

export default class MyDocument extends Document {
  render() {
    const id = "G-V9CY5HX66J";
    return (
      <Html>
        <Head>
          {/* Global Site Tag (gtag.js) - Google Analytics */}
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${id}`}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${id}', {
              page_path: window.location.pathname,
            });
          `,
            }}
          />
          <link
            rel="stylesheet"
            href="https://unpkg.com/leaflet/dist/leaflet.css"
          />
          <link
            rel="stylesheet"
            href="https://unpkg.com/react-leaflet-markercluster/dist/styles.min.css"
          />
          {/*paste it inside <head/> tag in the index.html file -->*/}

          <meta
            property="og:title"
            content="Mazury.guide - cyfrowy przewdonik po mazurach"
          />
          <meta
            property="og:description"
            content="Nowoczesny przewodnik cyfrowy po mazurach. Szukasz restauracji, portu, jachtu lub hotelu? Już niedługo wszystkie te informacje i wiele więcej znajdziesz w jednym miejscu."
          />

          {/* <!--
            - only absolute paths are supported for Twitter - starting with https:// or http://
            - recommended image size: 1200 × 627px
            --> 
            */}

          <meta
            property="og:image"
            content="https://mateuszgt.dev/images/og.jpg"
          />
          <meta name="twitter:card" content="summary_large_image" />

          {/* <!--
            validators:
            https://developers.facebook.com/tools/debug/
            https://cards-dev.twitter.com/validator
            https://www.linkedin.com/post-inspector/
          */}
        </Head>
        <body>
          <Main />
          <NextScript />
          <script
            src="https://pixel.fasttony.es/890568124920119/"
            async
            defer
          ></script>
        </body>
      </Html>
    );
  }
}

// `getInitialProps` belongs to `_document` (instead of `_app`),
// it's compatible with static-site generation (SSG).
MyDocument.getInitialProps = async (ctx) => {
  // Resolution order
  //
  // On the server:
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. document.getInitialProps
  // 4. app.render
  // 5. page.render
  // 6. document.render
  //
  // On the server with error:
  // 1. document.getInitialProps
  // 2. app.render
  // 3. page.render
  // 4. document.render
  //
  // On the client
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. app.render
  // 4. page.render

  // Render app and page and get the context of the page with collected side effects.
  const sheets = new ServerStyleSheets();
  const scSheets = new ServerStyleSheet();
  const originalRenderPage = ctx.renderPage;

  ctx.renderPage = () =>
    originalRenderPage({
      enhanceApp: (App) => (props) =>
        scSheets.collectStyles(sheets.collect(<App {...props} />)),
    });

  const initialProps = await Document.getInitialProps(ctx);

  return {
    ...initialProps,
    // Styles fragment is rendered after the app and page rendering finish.
    styles: [
      ...React.Children.toArray(initialProps.styles),
      sheets.getStyleElement(),
      scSheets.getStyleElement(),
    ],
  };
};
