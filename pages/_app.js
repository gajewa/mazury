import "../styles.css";
import { AppWrapper } from "../context/state"; // import based on where you put it
import { QueryClient, QueryClientProvider } from "react-query";
import CookieConsent from "react-cookie-consent";
import zIndex from "@material-ui/core/styles/zIndex";
import Head from "next/head";

const queryClient = new QueryClient();

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <Head>
        <title>Mazury.guide, całe mazury w jedny miejscu - na mapie</title>
      </Head>
      <AppWrapper>
        <Component {...pageProps} />
      </AppWrapper>
      <CookieConsent
        location="bottom"
        buttonText="Rozumiem"
        cookieName="mazury-guide-cookie-consent"
        style={{ background: "#2B373B" }}
        buttonStyle={{ color: "#4e503b", fontSize: "13px" }}
        expires={150}
        style={{ zIndex: "999999999" }}
      >
        Ta strona używa plików cookie.
      </CookieConsent>
    </QueryClientProvider>
  );
}
