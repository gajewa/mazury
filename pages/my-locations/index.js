import {
  Card,
  List,
  ListItem,
  ListItemIcon,
  Snackbar,
  Typography,
} from "@material-ui/core";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import { useEffect, useState } from "react";
import LayoutWithoutMap from "../../components/LayoutWithoutMap";
import { useAppContext } from "../../context/state";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

const MyLocations = () => {
  const classes = useStyles();
  const [locations, setLocations] = useState([]);

  const { userData } = useAppContext();

  const [snackbarOpen, setSnackbarOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setSnackbarOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  useEffect(() => {
    if (!userData) {
      return;
    }

    axios
      .get(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/users/${userData.id}/locations`
      )
      .then((res) => setLocations(res.data))
      .catch((error) => {
        setSnackbarOpen(true);
      });
  }, [userData]);

  return (
    <LayoutWithoutMap>
      <Card style={{ margin: "2%", padding: "2%" }}>
        <Typography variant="h5">Moje lokalizacje</Typography>
        <div className={classes.root}>
          <List component="nav" aria-label="main mailbox folders">
            {locations.map((location) => (
              <ListItem>
                <ListItemLink href={`/my-locations/${location.slug}`}>
                  <ListItemIcon>
                    <img
                      src={
                        location.type === "PORT"
                          ? "/images/icons/map-anchor-1.svg"
                          : location.type === "CHARTER"
                          ? "/images/icons/map-boat.svg"
                          : location.type === "RESTAURANT"
                          ? "/images/icons/002-cutlery.svg"
                          : "/images/icons/question.svg"
                      }
                      height="30"
                    />
                  </ListItemIcon>
                  <ListItemText primary={location.name} />
                </ListItemLink>
              </ListItem>
            ))}
          </List>
        </div>
      </Card>
      <Snackbar open={snackbarOpen} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Nie udało się pobrać listy. Zaloguj się lub skontaktuj się z
          hello@mazury.guide.
        </Alert>
      </Snackbar>
    </LayoutWithoutMap>
  );
};

export default MyLocations;
