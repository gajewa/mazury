import {
  AppBar,
  Box,
  Card,
  Grid,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import SwipeableViews from "react-swipeable-views";
import PortDetailsForm from "../../../components/PortDetailsForm";
import { useAppContext } from "../../../context/state";
import LayoutWithoutMap from "../../../components/LayoutWithoutMap";
import GeneralInfoForm from "../../../components/GeneralInfoForm";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: "100%",
  },
}));

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const MyLocations = ({ location }) => {
  const router = useRouter();
  const { locationSlug } = router.query;

  function a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      "aria-controls": `full-width-tabpanel-${index}`,
    };
  }

  const classes = useStyles();
  const theme = useTheme();

  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const renderSecondTab = () => {
    return location.type === "CHARTER" ? (
      <TabPanel value={value} index={1} dir={theme.direction}>
        <Grid container spacing={2}>
          <ListItem>
            <ListItemLink href={`/my-locations/${locationSlug}/vessel/create`}>
              <ListItemIcon>
                <AddIcon />
              </ListItemIcon>
              <ListItemText primary={"Dodaj nowy jacht"} />
            </ListItemLink>
          </ListItem>
          {location &&
            location.vessels &&
            location.vessels.map((vessel) => (
              <ListItem>
                <ListItemSecondaryAction>
                  <IconButton
                    edge="end"
                    aria-label="edit"
                    href={`/my-locations/${locationSlug}/vessel/${vessel.id}`}
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    edge="end"
                    aria-label="delete"
                    onClick={() =>
                      axios
                        .delete(
                          `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/vessels/${vessel.id}`
                        )
                        .then((res) => fetchLocations())
                        .catch((error) => {
                          // setSnackbarOpen(true);
                        })
                    }
                  >
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
                <ListItemIcon>
                  <img src={"/images/icons/map-boat.svg"} height="30" />
                </ListItemIcon>
                <ListItemText
                  primary={vessel.type}
                  secondary={vessel.name ? vessel.name : "brak nazwy"}
                />
              </ListItem>
            ))}
        </Grid>
      </TabPanel>
    ) : (
      <PortDetailsForm location={location} />
    );
  };

  return (
    <LayoutWithoutMap>
      <Card style={{ margin: "2%", padding: "2%" }}>
        <Box marginBottom="1rem">
          <Typography variant="h4">Edycja lokalizacji</Typography>
        </Box>

        <div className={classes.root}>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
              aria-label="full width tabs example"
            >
              <Tab label="Informacje ogólne" {...a11yProps(0)} />
              <Tab
                label={
                  location.type === "CHARTER" ? "Jachty" : "Szczegóły portu"
                }
                {...a11yProps(1)}
              />
            </Tabs>
          </AppBar>
          <SwipeableViews
            axis={theme.direction === "rtl" ? "x-reverse" : "x"}
            index={value}
            onChangeIndex={handleChangeIndex}
          >
            <TabPanel value={value} index={0} dir={theme.direction}>
              {location && <GeneralInfoForm location={location} />}
            </TabPanel>
            <TabPanel value={value} index={1} dir={theme.direction}>
              {renderSecondTab()}
            </TabPanel>
          </SwipeableViews>
        </div>
      </Card>
    </LayoutWithoutMap>
  );
};

export async function getServerSideProps(context) {
  // Fetch data from external API
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/locations/slug/${context.params.locationSlug}`
  );
  const location = await res.json();

  // Pass data to the page via props
  return { props: { location } };
}

export default MyLocations;
