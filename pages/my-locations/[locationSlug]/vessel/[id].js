import {
  Box,
  Button,
  Card,
  Snackbar,
  Grid,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import LayoutWithoutMap from "../../../../components/LayoutWithoutMap";
import MuiAlert from "@material-ui/lab/Alert";
import VesselForm from "../../../../components/VesselForm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: "100%",
  },
}));

const MyLocations = ({ vessel, location }) => {
  // const [vessel, setVessel] = useState([]);
  // const router = useRouter();
  // const { id, locationSlug } = router.query;

  // useEffect(() => {
  //   axios
  //     .get(`${process.env.NEXT_PUBLIC_API_BASE_URL}/api/vessels/${id}`)
  //     .then((res) => setVessel(res.data))
  //     .catch((error) => {});
  // }, [id]);

  // const [location, setLocation] = useState([]);

  // useEffect(() => {
  //   axios
  //     .get(
  //       `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/locations/slug/${locationSlug}`
  //     )
  //     .then((res) => setLocation(res.data))
  //     .catch((error) => {});
  // }, [locationSlug]);

  return (
    <LayoutWithoutMap>
      <VesselForm vessel={vessel} location={location} />
    </LayoutWithoutMap>
  );
};

export async function getServerSideProps(context) {
  // Fetch data from external API
  const vesselRes = await fetch(
    `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/vessels/${context.params.id}`
  );
  const vessel = await vesselRes.json();

  // Fetch data from external API
  const locationRes = await fetch(
    `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/locations/slug/${context.params.locationSlug}`
  );
  const location = await locationRes.json();

  // Pass data to the page via props
  return { props: { vessel, location } };
}

export default MyLocations;
