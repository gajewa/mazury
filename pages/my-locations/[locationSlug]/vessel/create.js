import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import LayoutWithoutMap from "../../../../components/LayoutWithoutMap";
import VesselForm from "../../../../components/VesselForm";

const MyLocations = () => {
  const router = useRouter();
  const { locationSlug } = router.query;

  const [location, setLocation] = useState([]);

  useEffect(() => {
    axios
      .get(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/locations/slug/${locationSlug}`
      )
      .then((res) => setLocation(res.data))
      .catch((error) => {});
  }, [locationSlug]);

  return (
    <LayoutWithoutMap>
      <VesselForm location={location} />
    </LayoutWithoutMap>
  );
};

export default MyLocations;
