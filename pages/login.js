import { Box, Button, Card, Grid, TextField } from "@material-ui/core";
import axios from "axios";
import { useRouter } from "next/router";
import { Controller, useForm } from "react-hook-form";
import {
  FacebookLoginButton,
  GoogleLoginButton,
} from "react-social-login-buttons";
import LayoutWithoutMap from "../components/LayoutWithoutMap";
import LoginComponent from "../components/LoginComponent";
import { useAppContext } from "../context/state";

const Register = () => {
  const router = useRouter();

  const onLogin = () => {
    router.push("/");
  };

  return (
    <LayoutWithoutMap>
      <Card
        style={{
          width: "95%",
          maxWidth: "400px",
          margin: "auto",
          marginTop: "1rem",
          marginBottom: "1rem",
        }}
      >
        <LoginComponent onLogin={onLogin} />
      </Card>
    </LayoutWithoutMap>
  );
};

export default Register;
