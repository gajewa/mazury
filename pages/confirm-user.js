import {
  Box,
  Button,
  Card,
  Grid,
  TextField,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { useRouter } from "next/router";
import { Controller, useForm } from "react-hook-form";
import {
  FacebookLoginButton,
  GoogleLoginButton,
} from "react-social-login-buttons";
import LayoutWithoutMap from "../components/LayoutWithoutMap";
import { useAppContext } from "../context/state";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useEffect, useState } from "react";

const ConfirmUser = () => {
  const router = useRouter();
  const { token, email } = router.query;
  const [confirmSuccess, setConfirmSuccess] = useState();

  useEffect(() => {
    if (token && email) {
      axios
        .post(
          `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/users/confirm?token=${token}&email=${email}`
        )
        .then((res) => {
          setConfirmSuccess(true);
        })
        .catch((error) => setConfirmSuccess(false));
    }
  }, [token, email]);

  const renderText = () => {
    if (confirmSuccess) {
      return "Użytkownik poprawnie potwierdzony, możesz się teraz zalogować.";
    } else {
      return "Potwierdzenie użytkownika nie powiodło się. Upewnij się że poprawny link został użyty. Jeśli problem będzie powtarzał skontaktuj się z nami na hello@mazury.guide lub na Facebooku.";
    }
  };

  return (
    <LayoutWithoutMap>
      <Card
        style={{
          width: "95%",
          maxWidth: "700px",
          margin: "auto",
          marginTop: "1rem",
          marginBottom: "1rem",
        }}
      >
        <Box padding={"1rem"}>
          <Grid container spacing={2} style={{ width: "100%", margin: "auto" }}>
            <Grid align="center" item xs={12}>
              <h2>Potwierdzenie użytkownika</h2>
            </Grid>
            <Grid align="center" item xs={12}>
              {confirmSuccess === undefined ? (
                <CircularProgress />
              ) : (
                <Typography>{renderText()}</Typography>
              )}
            </Grid>
          </Grid>
        </Box>
      </Card>
    </LayoutWithoutMap>
  );
};

export default ConfirmUser;
