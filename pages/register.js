import {
  Box,
  Button,
  Card,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { useRouter } from "next/router";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import {
  FacebookLoginButton,
  GoogleLoginButton,
} from "react-social-login-buttons";
import MuiAlert from "@material-ui/lab/Alert";
import LayoutWithoutMap from "../components/LayoutWithoutMap";
import { useAppContext } from "../context/state";

const Register = () => {
  const { handleSubmit, control, getValues, reset, errors } = useForm();
  const router = useRouter();
  const [formSent, setFormSent] = useState(false);
  const [errorContent, setErrorContent] = useState(
    "Przepraszamy, wystąpił błąd."
  );

  const onSubmit = (data) => {
    axios
      .post(`${process.env.NEXT_PUBLIC_API_BASE_URL}/api/users/register`, data)
      .then((res) => {
        setFormSent(true);
      })
      .catch((error) => {
        setErrorSnackbarOpen(true);
        setErrorContent(error.response.data.message);
      });
  };

  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setErrorSnackbarOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  return (
    <LayoutWithoutMap>
      <Card
        style={{
          width: "95%",
          maxWidth: "400px",
          margin: "auto",
          marginTop: "1rem",
          marginBottom: "1rem",
        }}
      >
        <Box padding={"1rem"}>
          <Grid container spacing={2} style={{ width: "100%", margin: "auto" }}>
            <Grid align="center" item xs={12}>
              <h2>Rejestracja</h2>
            </Grid>
            {formSent ? (
              <>
                {" "}
                <Grid align="center" item xs={12}>
                  <Typography>
                    Użytkownik zarejestrowany. Aby móc się zalogować potwierdź
                    adres email przz kliknięcie w link wysłany na podany adres.
                  </Typography>
                </Grid>
              </>
            ) : (
              <>
                <Grid item xs={12}>
                  <Controller
                    name="name"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        variant="outlined"
                        label="Imię i nazwisko (opcjonalnie)"
                        fullWidth
                        {...field}
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Controller
                    name="email"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        variant="outlined"
                        label="Email"
                        fullWidth
                        {...field}
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Controller
                    name="password"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        variant="outlined"
                        label="Hasło"
                        fullWidth
                        type="password"
                        {...field}
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Controller
                    name="passwordSecond"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        variant="outlined"
                        label="Powtórz hasło"
                        fullWidth
                        type="password"
                        {...field}
                      />
                    )}
                  />
                </Grid>

                <Grid item align="center" xs={12}>
                  <Box paddingLeft={"0"}>
                    <Button
                      style={{ width: "98%" }}
                      variant="contained"
                      color="primary"
                      onClick={handleSubmit(onSubmit)}
                    >
                      Zatwierdź
                    </Button>
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <FacebookLoginButton
                    onClick={() =>
                      router.push(
                        `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/oauth2/authorization/facebook`
                      )
                    }
                  >
                    <span>Zaloguj przez Facebook</span>
                  </FacebookLoginButton>
                </Grid>
                <Grid item xs={12}>
                  <GoogleLoginButton
                    onClick={() =>
                      router.push(
                        `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/oauth2/authorization/google`
                      )
                    }
                  >
                    <span>Zaloguj przez Google</span>
                  </GoogleLoginButton>
                </Grid>
              </>
            )}
          </Grid>
        </Box>
      </Card>
      <Snackbar open={errorSnackbarOpen} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {errorContent}
        </Alert>
      </Snackbar>
    </LayoutWithoutMap>
  );
};

export default Register;
