import {
  Box,
  Button,
  Grid,
  Hidden,
  Snackbar,
  TextField,
} from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import MuiAlert from "@material-ui/lab/Alert";
import axios from "axios";
import dynamic from "next/dynamic";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import styled from "styled-components";
import CommodityIconWithDescription from "../../components/CommodityIconWithDescription";
import Layout from "../../components/Layout";
import { useAppContext } from "../../context/state";

const ItemDetails = () => {
  const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: "95%",
      margin: "auto",
    },
  }));

  const { userData } = useAppContext();

  const classes = useStyles();

  const router = useRouter();
  const { name } = router.query;
  const [shownItem, setShownItem] = useState();
  const { handleSubmit, control, getValues, reset, errors } = useForm();

  const [locationList, setLocationList] = useState([]);
  const additionalFilters = router.query;

  const emptyStringIfUndefined = (s) => (s ? s : "");

  useEffect(() => {
    axios
      .get(
        `${
          process.env.NEXT_PUBLIC_API_BASE_URL
        }/api/locations?locationType=${emptyStringIfUndefined(
          additionalFilters.type
        )}&name=${emptyStringIfUndefined(
          additionalFilters.search
        )}&vesselType=${emptyStringIfUndefined(
          additionalFilters.vesselType
        )}&maxVesselLength=${emptyStringIfUndefined(
          additionalFilters.maxVesselLength
        )}&minVesselLength=${emptyStringIfUndefined(
          additionalFilters.minVesselLength
        )}&maxVesselCrew=${emptyStringIfUndefined(
          additionalFilters.maxVesselCrew
        )}&minVesselCrew=${emptyStringIfUndefined(
          additionalFilters.minVesselCrew
        )}&licenceNeededForVessel=${emptyStringIfUndefined(
          additionalFilters.licenceNeededForVessel
        )}&vesselCategory=${emptyStringIfUndefined(
          additionalFilters.vesselCategory
        )}`
      )
      .then((res) => {
        setLocationList(res.data);
      });
  }, [additionalFilters]);

  useEffect(() => {
    if (name) {
      axios
        .get(
          `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/locations/slug/${name}/`
        )
        .then((res) => {
          setShownItem(res.data);
        });
    }
  }, [name]);

  let vesselsToShow = shownItem && shownItem.vessels;

  const handleClick = (data) => {
    axios
      .post(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/locations/${shownItem.id}/mail`,
        data
      )
      .then((res) => {
        setOpen(true);
      })
      .catch((error) => {
        // setErrorSnackbarOpen(true);
      });
  };

  const [snackbarOpen, setOpen] = useState(false);
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const renderPrices = (ves) => {
    if (ves.pricePerDayMax && ves.pricePerDayMin) {
      if (ves.pricePerDayMax === ves.pricePerDayMin) {
        return `Cena: ${ves.pricePerDayMax}zł / doba`;
      }

      return `Cena: ${ves.pricePerDayMin}zł - ${ves.pricePerDayMax}zł / doba`;
    }

    if (ves.pricePerDayMin) {
      return `Cena: od ${ves.pricePerDayMin} zł / doba`;
    }

    if (ves.pricePerDayMax) {
      return `Cena: do ${ves.pricePerDayMax} zł / doba`;
    }

    return "";
  };

  return (
    <>
      {shownItem ? (
        <Layout
          startLat={shownItem.lat}
          startLen={shownItem.lang}
          startZoom={15}
          hideDetailsMapOnSmallScreens={true}
          locationList={
            locationList.indexOf(shownItem) === -1
              ? [...locationList, shownItem]
              : locationList
          }
        >
          <Hidden lgUp>
            <Grid item xs={12} md={3} align="center">
              <GeographicalMapWithNoSsr
                data={[...locationList, shownItem]}
                startLat={shownItem.lat}
                startLen={shownItem.lang}
                startZoom={14}
                reduecedMap
              />
            </Grid>
          </Hidden>
          <StyledCard>
            <CardContent>
              <Box
                mb={"10rem"}
                position="absolute"
                marginTop={0}
                zIndex={9999999}
                style={{
                  opacity: "0.5",
                }}
              >
                <Link href="/">
                  <a>
                    <ArrowBackIcon />
                  </a>
                </Link>
              </Box>
              <CenteredTitle>{shownItem.name}</CenteredTitle>
              <Box mt={"2rem"}>
                <Grid
                  container
                  alignItems="center"
                  justify="center"
                  spacing={0}
                >
                  <Grid item xs={12} md={6} align="center">
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                      style={{ margin: "2%" }}
                      align="center"
                    >
                      <Box display="flex" justifyContent="center">
                        <img height={"25rem"} src={`/images/map.png`} />
                        <Box ml={"1rem"} mt={"0.3rem"}>
                          {`${shownItem.locationCity}, ${shownItem.locationStreet}`}
                        </Box>
                      </Box>
                    </Typography>
                  </Grid>
                  <Grid item xs={12} md={6} align="center">
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                      style={{ margin: "2%" }}
                      align="center"
                    >
                      <Box display="flex" justifyContent="center">
                        <img
                          height={"25rem"}
                          src={`/images/icons/webpage.svg`}
                        />
                        <Box ml={"1rem"} mt={"0.1rem"}>
                          <a href={shownItem.website} target="_blank">
                            {shownItem.websiteLabel}
                          </a>
                        </Box>
                      </Box>
                    </Typography>
                  </Grid>
                  <Grid item xs={12} md={6} align="center">
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                      style={{ margin: "2%" }}
                      align="center"
                    >
                      <Box display="flex" justifyContent="center">
                        <img height={"25rem"} src={`/images/icons/phone.svg`} />
                        <Box ml={"1rem"} mt={"0.2rem"}>
                          {`${
                            shownItem.phoneNumber ? shownItem.phoneNumber : "?"
                          }`}
                        </Box>
                      </Box>
                    </Typography>
                  </Grid>
                  <Grid item xs={12} md={6} align="center">
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                      style={{ margin: "2%" }}
                      align="center"
                    >
                      <Box display="flex" justifyContent="center">
                        <img height={"25rem"} src={`/images/icons/email.svg`} />
                        <Box ml={"1rem"} mt={"0.2rem"}>
                          {`${shownItem.email ? shownItem.email : "?"}`}
                        </Box>
                      </Box>
                    </Typography>
                  </Grid>

                  {shownItem.type === "RESTAURANT" && (
                    <Grid item xs={12} md={6} align="center">
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                        style={{ margin: "2%" }}
                        align="center"
                      >
                        <Box display="flex" justifyContent="center">
                          <img
                            height={"25rem"}
                            src={`/images/icons/open.svg`}
                          />
                          <Box ml={"1rem"} mt={"0.3rem"}>
                            {`${shownItem.openTime} - ${shownItem.closeTime}`}
                          </Box>
                        </Box>
                      </Typography>
                    </Grid>
                  )}
                </Grid>
              </Box>

              {shownItem.type === "PORT" ? (
                <>
                  <Box mt={"3rem"}>
                    <Grid
                      container
                      alignItems="center"
                      justify="center"
                      spacing={2}
                    >
                      <Grid item xs={4} md={3} align="center">
                        <CommodityIconWithDescription
                          iconName={"021-swimming-pool.svg"}
                          descriptionIfAvailable={`${shownItem.depthMin}m - ${shownItem.depthMax}m`}
                          isCommodityAvailable
                        />
                      </Grid>
                      <Grid item xs={4} md={3} align="center">
                        <CommodityIconWithDescription
                          iconName={"017-anchor-1.svg"}
                          descriptionIfAvailable={`${shownItem.stopTypes
                            .map((st) => st.name)
                            .join(", ")}`}
                          isCommodityAvailable
                        />
                      </Grid>
                      <Grid item xs={4} md={3} align="center">
                        <CommodityIconWithDescription
                          iconName={"wallet.svg"}
                          descriptionIfAvailable={`${
                            shownItem.pricePerVessel
                          }zł + ${
                            shownItem.pricePerPerson
                              ? shownItem.pricePerPerson
                              : 0
                          }zł/os.`}
                          isCommodityAvailable
                        />
                      </Grid>
                      <Grid xs={12} align="center">
                        <Typography
                          variant="body2"
                          color="textSecondary"
                          component="p"
                          style={{ margin: "5%" }}
                          align="center"
                        >
                          {shownItem.description}
                        </Typography>
                      </Grid>
                      <Grid xs={12} align="center">
                        <Subtitle>Dostępne udogodnienia:</Subtitle>
                      </Grid>
                      <Grid item xs={4} md={2} align="center">
                        <CommodityIconWithDescription
                          isCommodityAvailable={shownItem.electricity}
                          iconName={"004-lightning.svg"}
                          descriptionIfAvailable={`${shownItem.electricityPrice}zł`}
                          descriptionIfAvailable={
                            shownItem.electricityPrice
                              ? `${shownItem.electricityPrice}zł`
                              : "dostępne"
                          }
                          small
                        />
                      </Grid>
                      <Grid item xs={4} md={2} align="center">
                        <CommodityIconWithDescription
                          isCommodityAvailable={shownItem.shower}
                          iconName={"006-shower-1.svg"}
                          descriptionIfAvailable={`${shownItem.showerPrice}zł`}
                          descriptionIfAvailable={
                            shownItem.showerPrice
                              ? `${shownItem.showerPrice}zł`
                              : "dostępne"
                          }
                          small
                        />
                      </Grid>
                      <Grid item xs={4} md={2} align="center">
                        <CommodityIconWithDescription
                          isCommodityAvailable={shownItem.shop}
                          iconName={"009-shopping-cart-2.svg"}
                          descriptionIfAvailable={
                            shownItem.shopDistance
                              ? `${shownItem.shopDistance}zł`
                              : "dostępne"
                          }
                          small
                        />
                      </Grid>
                      <Grid item xs={4} md={2} align="center">
                        <CommodityIconWithDescription
                          isCommodityAvailable={shownItem.toilet}
                          iconName={"013-toilet-2.svg"}
                          descriptionIfAvailable={
                            shownItem.toiletPrice
                              ? `${shownItem.toiletPrice}zł`
                              : "dostępne"
                          }
                          small
                        />
                      </Grid>
                      <Grid item xs={4} md={2} align="center">
                        <CommodityIconWithDescription
                          isCommodityAvailable={shownItem.water}
                          iconName={"015-drop-1.svg"}
                          descriptionIfAvailable={
                            shownItem.waterPrice
                              ? `${shownItem.waterPrice}zł`
                              : "dostępne"
                          }
                          small
                        />
                      </Grid>
                      <Grid item xs={4} md={2} align="center">
                        <CommodityIconWithDescription
                          isCommodityAvailable={shownItem.bar}
                          iconName={"002-cutlery.svg"}
                          descriptionIfAvailable={
                            shownItem.barDistance
                              ? `${shownItem.barDistance}m`
                              : "dostępne"
                          }
                          small
                        />
                      </Grid>
                    </Grid>
                  </Box>
                </>
              ) : (
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  style={{ margin: "5%" }}
                  align="center"
                >
                  {shownItem.description}
                </Typography>
              )}
            </CardContent>
          </StyledCard>

          {shownItem.type === "CHARTER" ? (
            <>
              <CenteredTitle>Dostępne jednostki</CenteredTitle>
              {vesselsToShow.map((ves) => (
                <StyledCard key={ves.type} className={classes.listCardRoot}>
                  <StyledAnchor
                    onClick={() => console.log("you clicked the anchor!")}
                  >
                    <StyledImg
                      src={
                        ves.category === "SAILBOAT"
                          ? "/images/sailing-yacht.jpg"
                          : "/images/speedboat.jpg"
                      }
                    />
                    <InformationWrapper>
                      <Title>{ves.type}</Title>
                      {ves.name && (
                        <VesselNameSubtitle>{ves.name}</VesselNameSubtitle>
                      )}
                      <Typography>{renderPrices(ves)}</Typography>
                      <Typography>Długość: {ves.length}m</Typography>
                      <Typography>Szerokość: {ves.width}m</Typography>
                      <Typography>Zanurzenie: {ves.immersion}m</Typography>
                    </InformationWrapper>
                  </StyledAnchor>
                </StyledCard>
              ))}

              {shownItem.contactFormActive && (
                <StyledCard>
                  <Box mb={"1rem"}>
                    <Grid container spacing={2}>
                      <form style={{ width: "100%" }}>
                        <Grid item xs={12} align="center">
                          <Box mb="1rem">
                            <CenteredTitle>Zapytaj o jacht</CenteredTitle>
                          </Box>
                        </Grid>
                        <Grid item xs={12} align="center">
                          <Box mb="1rem" width="85%">
                            <Controller
                              name="name"
                              control={control}
                              defaultValue={userData?.name}
                              render={({ field }) => (
                                <TextField
                                  variant="outlined"
                                  multiline
                                  fullWidth
                                  label="Imię i nazwisko"
                                  {...field}
                                />
                              )}
                            />
                          </Box>
                        </Grid>
                        <Grid item xs={12} align="center">
                          <Box mb="1rem" width="85%">
                            <Controller
                              name="email"
                              control={control}
                              defaultValue={userData?.email}
                              render={({ field }) => (
                                <TextField
                                  variant="outlined"
                                  multiline
                                  fullWidth
                                  label="Email"
                                  {...field}
                                />
                              )}
                            />
                          </Box>
                        </Grid>
                        <Grid item xs={12} align="center">
                          <Box mb="1rem" width="85%">
                            <Controller
                              name="phoneNumber"
                              control={control}
                              render={({ field }) => (
                                <TextField
                                  variant="outlined"
                                  multiline
                                  fullWidth
                                  label="Numer telefonu"
                                  {...field}
                                />
                              )}
                            />
                          </Box>
                        </Grid>
                        <Grid item xs={12} align="center">
                          <Box mb="1rem" width="85%">
                            <Controller
                              name="content"
                              control={control}
                              render={({ field }) => (
                                <TextField
                                  variant="outlined"
                                  multiline
                                  fullWidth
                                  minRows={3}
                                  label="Treść"
                                  {...field}
                                />
                              )}
                            />
                          </Box>
                        </Grid>
                        <Grid item xs={12} align="center">
                          <Box mb="1rem" width="95%">
                            <Button
                              variant="contained"
                              color="primary"
                              style={{ width: "90%" }}
                              type="reset"
                              onClick={handleSubmit(handleClick)}
                            >
                              Wyślij
                            </Button>
                          </Box>
                        </Grid>
                      </form>
                    </Grid>
                  </Box>
                </StyledCard>
              )}
              <Snackbar
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
              >
                <Alert onClose={handleClose} severity="success">
                  Email sent!
                </Alert>
              </Snackbar>
            </>
          ) : (
            <></>
          )}
        </Layout>
      ) : (
        <></>
      )}
    </>
  );
};

const Title = styled.h1`
  color: #29343d;
  padding: 0px;
  margin-top: 1.5rem;
  margin-bottom: 0.5rem;
`;

const CenteredTitle = styled.h1`
  color: #29343d;
  padding: 0px;
  margin-top: 1.5rem;
  margin-bottom: 0.5rem;
  text-align: center;
`;

const VesselNameSubtitle = styled.h3`
  color: #29343d;
  padding: 0px;
  margin-top: 0rem;
  margin-bottom: 1rem;
`;

const Subtitle = styled.h5`
  font-size: 25px;
  color: #29343d;
  padding: 0px;
  text-align: center;
  margin-top: 1rem;
  margin-bottom: 1rem;
`;

const StyledCard = styled.div`
  background-color: white;
  box-shadow: 0px 8px 24px -2px rgba(0, 0, 0, 0.08);
  border-radius: 12px;
  overflow: hidden;
  transition: all 0.2s ease-in;

  :hover {
    transform: translateY(-4px);
    box-shadow: 0px 8px 24px -2px rgba(0, 0, 0, 0.16);
  }
  max-width: 95%;
  margin: auto;
  margin-bottom: 1rem;
  margin-top: 1rem;
`;

const StyledAnchor = styled.a`
  color: #7896a5 !important;
  width: 100%;
  display: flex;
  :hover {
    cursor: pointer;
  }

  @media (max-width: 624px) {
    flex-direction: column;
  }
`;
const StyledImg = styled.img`
  overflow: hidden;
  max-width: 400px;
  min-width: 160px;
  height: 100%;
  width: 100%;
  object-fit: cover;
`;

const InformationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px 16px 16px 24px;
  margin: 0px;
`;

const GeographicalMapWithNoSsr = dynamic(
  () => import("../../components/GeographicalMap"),
  {
    ssr: false,
  }
);

export default ItemDetails;
