import { useCallback, useEffect, useRef, useState } from "react";
import { useAppContext } from "../context/state";
import LocationListElement from "./LocationListElement";
import { VariableSizeList as List } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import { useWindowResize } from "../lib/ga/useWindowResize";
import axios from "axios";

const YachtList = ({ locationList }) => {
  const Row = ({ data, index, setSize, windowWidth }) => {
    const rowRef = useRef();

    useEffect(() => {
      setSize(index, rowRef.current.getBoundingClientRect().height + 15);
    }, [setSize, index, windowWidth]);

    return <div ref={rowRef}>{<LocationListElement item={data[index]} />}</div>;
  };

  const listRef = useRef();
  const sizeMap = useRef({});
  const setSize = useCallback((index, size) => {
    sizeMap.current = { ...sizeMap.current, [index]: size };
    listRef.current.resetAfterIndex(index);
  }, []);
  const getSize = (index) => sizeMap.current[index] || 250;
  const [windowWidth] = useWindowResize();

  return (
    <>
      <AutoSizer>
        {({ height, width }) => (
          <List
            ref={listRef}
            className="List"
            height={height}
            itemCount={locationList.length}
            itemSize={getSize}
            width={width}
            itemData={locationList}
          >
            {({ data, index, style }) => (
              <div style={style}>
                <Row
                  data={data}
                  index={index}
                  setSize={setSize}
                  windowWidth={windowWidth}
                />
              </div>
            )}
          </List>
        )}
      </AutoSizer>
    </>
  );
};

export default YachtList;
