import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  Hidden,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  TextField,
  Typography,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import SendIcon from "@material-ui/icons/Send";
import dynamic from "next/dynamic";
import Link from "next/link";
import { Router, useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { Link as ScrollLink } from "react-scroll";
import styled from "styled-components";
import { useAppContext } from "../context/state";
import LoginComponent from "./LoginComponent";
import MobileDrawer from "./MobileDrawer";
import { DebounceInput } from "react-debounce-input";
import Select from "react-select";

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    "&:focus": {
      backgroundColor: theme.palette.primary.main,
      "& .MuiListItemIcon-root, & .MuiListItemText-primary": {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

const Layout = ({
  children,
  startLat = 53.91541912643642,
  startLen = 21.765518560894712,
  startZoom = 9.2,
  hideDetailsMapOnSmallScreens: detailsView = false,
  hideOverflowY = false,
  locationList,
}) => {
  const {
    userData,
    locationTypeToShow,
    setLocationTypeToShow,
    locationNameQuery,
    setLocationNameQuery,
  } = useAppContext();

  const router = useRouter();

  const { type } = router.query;

  setLocationTypeToShow(type ? type : "");

  const { control, getValues } = useForm(router.query);

  const [scrollPosition, setScrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const [filterDialogOpen, setFilterDialogOpen] = useState(false);

  const handleCloseFilterDialog = () => {
    const vals = getValues();

    const query = router.query;

    if (vals.vesselType) {
      query.vesselType = vals.vesselType;
    } else {
      delete query.vesselType;
    }

    if (vals.maxVesselLength) {
      query.maxVesselLength = vals.maxVesselLength;
    } else {
      delete query.maxVesselLength;
    }

    if (vals.minVesselLength) {
      query.minVesselLength = vals.minVesselLength;
    } else {
      delete query.minVesselLength;
    }

    if (vals.licenceNeededForVessel && vals.licenceNeededForVessel.value) {
      query.licenceNeededForVessel = vals.licenceNeededForVessel?.value;
    } else {
      delete query.licenceNeededForVessel;
    }

    if (vals.maxVesselCrew) {
      query.maxVesselCrew = vals.maxVesselCrew;
    } else {
      delete query.maxVesselCrew;
    }

    if (vals.minVesselCrew) {
      query.minVesselCrew = vals.minVesselCrew;
    } else {
      delete query.minVesselCrew;
    }

    if (vals.vesselCategory && vals.vesselCategory.value) {
      query.vesselCategory = vals.vesselCategory.value;
    } else {
      delete query.vesselCategory;
    }

    setFilterDialogOpen(false);
    router.push({
      path: router.path,
      query: query,
    });
  };

  const handleOpenFilterDialog = () => {
    setFilterDialogOpen(true);
  };

  const ListWrapper = styled.div`
    background-color: #f0f0f0;
    ${hideOverflowY ? "overflow-y: hidden" : ""}
    overflow-x: hidden;
    height: 90vh;
    width: 100%;
  `;

  const DetailsWrapper = styled.div`
    background-color: #f0f0f0;
    overflow-y: scroll;
    overflow-x: hidden;
    height: 90vh;
    width: 100%;
  `;

  const ContentWrapper = styled.div`
    height: 90vh;
    width: 100%;
  `;

  return (
    <>
      <Grid container spacing={0}>
        <Grid item xs={2} lg={1}>
          <Hidden mdDown>
            <LogoWrapper>
              <Link href="/">
                <a>
                  <img src="/images/logo.png" />
                </a>
              </Link>
            </LogoWrapper>
          </Hidden>
          <Hidden lgUp>
            <LogoWrapper>
              <Link href="/">
                <a>
                  <img src="/favicon.ico" />
                </a>
              </Link>
            </LogoWrapper>
          </Hidden>
        </Grid>
        <Grid item xs={8} lg={4}>
          <DebounceInput
            placeholder="Szukaj miejsca..."
            element={StyledInput}
            debounceTimeout={800}
            onChange={(event) => {
              const query = router.query;
              query.search = event.target.value;
              router.push({
                path: router.path,
                query: query,
              });
            }}
          />
        </Grid>
        <Hidden mdDown>
          <Grid item xs={5}>
            <Box display="flex">
              <Box marginTop={"1.5rem"} marginLeft={"1.5rem"} display="flex">
                <Typography>Filtruj:</Typography>
              </Box>
              <Box marginTop={"1.4rem"} marginLeft={"0.5rem"}>
                <img
                  src="/images/icons/all.svg"
                  height="30"
                  style={{
                    opacity: locationTypeToShow !== "" ? "0.25" : "1",
                  }}
                  onClick={() => {
                    const query = router.query;
                    delete query.type;
                    router.push({
                      path: router.path,
                      query: query,
                    });
                  }}
                />
              </Box>
              <Box marginTop={"1.4rem"} marginLeft={"0.5rem"} display="flex">
                <img
                  src="/images/icons/map-anchor-1.svg"
                  height="30"
                  style={{
                    opacity:
                      locationTypeToShow !== "" && locationTypeToShow !== "PORT"
                        ? "0.25"
                        : "1",
                  }}
                  onClick={() => {
                    const query = router.query;
                    query.type = "PORT";
                    router.push({
                      path: router.path,
                      query: query,
                    });
                  }}
                />
              </Box>
              <Box marginTop={"1.4rem"} marginLeft={"0.5rem"} display="flex">
                <img
                  src="/images/icons/map-boat.svg"
                  height="30"
                  style={{
                    opacity:
                      locationTypeToShow !== "" &&
                      locationTypeToShow !== "CHARTER"
                        ? "0.25"
                        : "1",
                  }}
                  onClick={() => {
                    const query = router.query;
                    query.type = "CHARTER";
                    router.push({
                      path: router.path,
                      query: query,
                    });
                  }}
                />
              </Box>
              <Box marginTop={"1.4rem"} marginLeft={"0.5rem"} display="flex">
                <img
                  src="/images/icons/002-cutlery.svg"
                  height="30"
                  style={{
                    opacity:
                      locationTypeToShow !== "" &&
                      locationTypeToShow !== "RESTAURANT"
                        ? "0.25"
                        : "1",
                  }}
                  onClick={() => {
                    const query = router.query;
                    query.type = "RESTAURANT";
                    router.push({
                      path: router.path,
                      query: query,
                    });
                  }}
                />
              </Box>
              <Box marginTop={"1.4rem"} marginLeft={"0.5rem"} display="flex">
                <img
                  src="/images/icons/question.svg"
                  height="30"
                  style={{
                    opacity:
                      locationTypeToShow !== "" &&
                      locationTypeToShow !== "OTHER"
                        ? "0.25"
                        : "1",
                  }}
                  onClick={() => {
                    const query = router.query;
                    query.type = "OTHER";
                    router.push({
                      path: router.path,
                      query: query,
                    });
                  }}
                />
              </Box>
              {locationTypeToShow === "CHARTER" && (
                <Box marginTop={"1.4rem"} marginLeft={"1.5rem"} display="flex">
                  <Button
                    sx={{ borderRadius: 78 }}
                    variant="outlined"
                    size="small"
                    onClick={handleOpenFilterDialog}
                  >
                    CECHY JACHTU
                  </Button>
                </Box>
              )}
            </Box>
          </Grid>
          <Grid
            item
            xs={2}
            align="right"
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            {userData !== undefined ? (
              <div>
                <Box marginTop={"1rem"}>
                  <Button
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    variant="contained"
                    color="primary"
                    onClick={handleClick}
                  >
                    Witaj {userData?.name?.split(" ")[0]}{" "}
                    {userData.admin && "▼"}
                  </Button>
                  {userData.admin && (
                    <StyledMenu
                      id="customized-menu"
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleClose}
                    >
                      <StyledMenuItem>
                        <Link href="/my-locations">
                          <a>
                            <ListItemIcon>
                              <SendIcon fontSize="small" />
                            </ListItemIcon>
                            <ListItemText primary="Moje lokalizacje" />
                          </a>
                        </Link>
                      </StyledMenuItem>
                    </StyledMenu>
                  )}
                </Box>
              </div>
            ) : (
              <Box marginTop={"1rem"}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleClick}
                >
                  Zaloguj ▼
                </Button>
                <StyledMenu
                  id="customized-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
                  <Box width="30vw">
                    <LoginComponent onLogin={handleClose} />
                  </Box>
                </StyledMenu>
              </Box>
            )}
            <Box
              marginTop={"1.5rem"}
              marginX={"0.5rem"}
              style={{
                opacity: "0.5",
              }}
            >
              <Link href="/terms">
                <a>
                  <HelpOutlineIcon />
                </a>
              </Link>
            </Box>
          </Grid>
        </Hidden>
        <MobileDrawer
          submitFilters={handleCloseFilterDialog}
          control={control}
          getValues={getValues}
        />
        <Grid item xs={12} md={12} lg={6} id="map">
          {detailsView ? (
            <>
              <Hidden mdDown>
                <ContentWrapper>
                  <GeographicalMapWithNoSsr
                    data={locationList}
                    startLat={startLat}
                    startLen={startLen}
                    startZoom={startZoom}
                  />
                </ContentWrapper>
              </Hidden>
            </>
          ) : (
            <ContentWrapper>
              <GeographicalMapWithNoSsr
                data={locationList}
                startLat={startLat}
                startLen={startLen}
                startZoom={startZoom}
              />
            </ContentWrapper>
          )}
        </Grid>
        <Grid item xs={12} md={12} lg={6} id="list">
          {detailsView ? (
            <DetailsWrapper>{children}</DetailsWrapper>
          ) : (
            <ListWrapper>{children}</ListWrapper>
          )}
        </Grid>
      </Grid>

      <Dialog open={filterDialogOpen} onClose={handleCloseFilterDialog}>
        <DialogTitle>Cechy jachtu</DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Controller
                name="minVesselLength"
                control={control}
                defaultValue={router.query.minVesselLength}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label="Min. długość"
                    size="small"
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="maxVesselLength"
                defaultValue={router.query.maxVesselLength}
                control={control}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label="Max. długość"
                    size="small"
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="minVesselCrew"
                control={control}
                defaultValue={router.query.minVesselCrew}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label="Min ilość załogi"
                    size="small"
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name="maxVesselCrew"
                control={control}
                defaultValue={router.query.maxVesselCrew}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label="Max ilość załogi"
                    size="small"
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ field }) => (
                  <Select
                    {...field}
                    isClearable
                    menuPortalTarget={document.body}
                    defaultValue={[
                      { label: "Patent wymagany", value: "true" },
                      {
                        label: "Patent niewymagany",
                        value: "false",
                      },
                    ].find(
                      (el) => el.value === router.query.licenceNeededForVessel
                    )}
                    styles={{
                      menuPortal: (base) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                    placeholder="Patent"
                    options={[
                      { label: "Patent wymagany", value: "true" },
                      {
                        label: "Patent niewymagany",
                        value: "false",
                      },
                    ]}
                  />
                )}
                name="licenceNeededForVessel"
                control={control}
                rules={{ required: true }}
                styles={{
                  menuPortal: (base) => ({
                    ...base,
                    zIndex: 9999,
                  }),
                }}
                menuPortalTarget={document.body}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                render={({ field }) => (
                  <Select
                    {...field}
                    isClearable
                    menuPortalTarget={document.body}
                    defaultValue={[
                      { label: "Żaglowy", value: "SAILBOAT" },
                      {
                        label: "Motorowy / Houseboat",
                        value: "MOTORBOAT",
                      },
                    ].find((el) => el.value === router.query.vesselCategory)}
                    styles={{
                      menuPortal: (base) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                    placeholder="Typ jachtu"
                    options={[
                      { label: "Żaglowy", value: "SAILBOAT" },
                      {
                        label: "Motorowy / Houseboat",
                        value: "MOTORBOAT",
                      },
                    ]}
                  />
                )}
                name="vesselCategory"
                control={control}
                rules={{ required: true }}
                menu
                styles={{
                  menuPortal: (base) => ({
                    ...base,
                    zIndex: 9999,
                  }),
                }}
                menuPortalTarget={document.body}
              />
            </Grid>
          </Grid>
        </DialogContent>

        <DialogActions>
          <Button onClick={handleCloseFilterDialog}>Szukaj</Button>
        </DialogActions>
      </Dialog>

      {!detailsView && (
        <Hidden lgUp>
          <Box
            left={0}
            right={0}
            width={"80px"}
            bottom={"35px"}
            marginX={"auto"}
            marginY={"0"}
            display={"flex"}
            position={"fixed"}
            zIndex={9999999}
            borderRadius={"10px"}
            style={{ backgroundColor: "black" }}
            color={"white"}
            padding={"5px"}
            justifyContent={"center"}
          >
            <>
              {scrollPosition > 300 ? (
                <ScrollLink
                  activeClass="active"
                  to="map"
                  spy={true}
                  smooth={true}
                  offset={-70}
                  duration={500}
                >
                  ↑ Map
                </ScrollLink>
              ) : (
                <ScrollLink
                  activeClass="active"
                  to="list"
                  spy={true}
                  smooth={true}
                  offset={-70}
                  duration={500}
                >
                  {" "}
                  ↓ List
                </ScrollLink>
              )}
            </>
          </Box>
        </Hidden>
      )}
    </>
  );
};

export default Layout;

const GeographicalMapWithNoSsr = dynamic(() => import("./GeographicalMap"), {
  ssr: false,
});

const LogoWrapper = styled.div`
  margin: 1rem;
  img {
    height: 5vh;
  }
`;

const StyledInput = styled.input`
  width: 95%;
  border-radius: 0.5rem;
  margin: 0.8rem 2%;
  border: none;
  box-shadow: 0px 8px 24px -2px rgba(0, 0, 0, 0.08);
  font-size: 1rem;
  padding: 0.7rem;
  border: 1px solid rgba(0, 0, 0, 0.04);
  transition: all 0.2s ease-in;
  color: #7896a5;

  :hover {
    border: 1px solid rgba(3, 126, 243, 0.4);
  }

  :focus {
    box-shadow: 0px 8px 24px -2px rgba(3, 126, 243, 0.16);
    color: #29343d;
    border: none;
    text-decoration: none;
    outline: none;
    border: 1px solid rgba(3, 126, 243, 0.8);
    caret-color: rgb(3, 126, 243);
  }
`;
