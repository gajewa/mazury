import { Box, Button, Grid, TextField } from "@material-ui/core";
import axios from "axios";
import { useRouter } from "next/router";
import { Controller, useForm } from "react-hook-form";
import {
  FacebookLoginButton,
  GoogleLoginButton,
} from "react-social-login-buttons";
import { useAppContext } from "../context/state";
import Link from "next/link";

const LoginComponent = ({ onLogin }) => {
  const { handleSubmit, control, getValues, reset, errors } = useForm();
  const { setToken } = useAppContext();
  const router = useRouter();

  const onSubmit = (data) => {
    axios
      .post(`${process.env.NEXT_PUBLIC_API_BASE_URL}/api/users/login`, data)
      .then((res) => {
        setToken(res.data.token);
        // cookieCutter.set("mazuryGuideToken", res.data.token);
        onLogin();
      })
      .catch((error) => {});
  };

  return (
    <>
      <Box padding={"1rem"} margin={"auto"}>
        <Grid container spacing={2} style={{ width: "100%", margin: 0 }}>
          <Grid item xs={12}>
            <Controller
              name="email"
              control={control}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  label="Email"
                  fullWidth
                  {...field}
                />
              )}
            />
          </Grid>

          <Grid item xs={12}>
            <Controller
              name="password"
              control={control}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  label="Hasło"
                  fullWidth
                  type="password"
                  {...field}
                />
              )}
            />
          </Grid>

          <Grid item xs={12}>
            <Box align="center" paddingLeft={"0rem"}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleSubmit(onSubmit)}
                style={{ width: "75%" }}
              >
                Zaloguj
              </Button>
            </Box>
          </Grid>
          <Grid align="center" item xs={12}>
            <Box padding={"0.5rem"}>
              <Link href="/register">
                <a>Nie masz konta? Zarejestruj się</a>
              </Link>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <FacebookLoginButton
              onClick={() =>
                router.push(
                  `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/oauth2/authorization/facebook`
                )
              }
            >
              <span>Zaloguj przez Facebook</span>
            </FacebookLoginButton>
          </Grid>
          <Grid item xs={12}>
            <GoogleLoginButton
              onClick={() =>
                router.push(
                  `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/oauth2/authorization/google`
                )
              }
            >
              <span>Zaloguj przez Google</span>
            </GoogleLoginButton>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default LoginComponent;
