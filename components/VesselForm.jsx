import {
  Box,
  Button,
  Card,
  Snackbar,
  Grid,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Controller, useForm } from "react-hook-form";
import MuiAlert from "@material-ui/lab/Alert";
import { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";

const VesselForm = ({ vessel, location }) => {
  const { handleSubmit, control, getValues, reset, errors } = useForm();

  const router = useRouter();
  const { successSnackbar } = router.query;

  useEffect(() => {
    setSuccessSnackbarOpen(successSnackbar);
  }, []);

  const onSubmit = (data) => {
    if (vessel) {
      const id = vessel.id;
      axios
        .put(`${process.env.NEXT_PUBLIC_API_BASE_URL}/api/vessels/${id}`, data)
        .then((res) => {
          setSuccessSnackbarOpen(true);
        })
        .catch((error) => {
          setErrorSnackbarOpen(true);
        });
    } else {
      data.locationId = location.id;
      axios
        .post(`${process.env.NEXT_PUBLIC_API_BASE_URL}/api/vessels`, data)
        .then((res) => {
          setSuccessSnackbarOpen(true);
          router.push(
            `/my-locations/${location.slug}/vessel/${res.data.id}?successSnackbar=true`
          );
        })
        .catch((error) => {
          setErrorSnackbarOpen(true);
        });
    }
  };

  const [successSnackbarOpen, setSuccessSnackbarOpen] = useState(false);
  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setSuccessSnackbarOpen(false);
    setErrorSnackbarOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  return (
    <>
      <Card style={{ margin: "2%", padding: "2%" }}>
        <Box marginBottom="1rem">
          <Typography variant="h4">
            {vessel ? "Szczegóły i edycja jachtu" : "Dodawanie jachtu"}
          </Typography>
        </Box>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography>Nazwa i typ</Typography>
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="type"
              control={control}
              defaultValue={vessel?.type}
              render={({ field }) => (
                <TextField
                  fullWidth
                  variant="outlined"
                  label="Typ"
                  {...field}
                />
              )}
            />
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="name"
              control={control}
              defaultValue={vessel?.name}
              render={({ field }) => (
                <TextField
                  fullWidth
                  variant="outlined"
                  label="Nazwa"
                  {...field}
                />
              )}
            />
          </Grid>
          <Grid item xs={0} md={6}></Grid>
          <Grid item xs={12}>
            <Typography>Ceny</Typography>
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="pricePerDayMin"
              control={control}
              defaultValue={vessel?.pricePerDayMin}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  fullWidth
                  label="Najmniejsza cena dzienna [zł]"
                  {...field}
                />
              )}
            />
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="pricePerDayMax"
              control={control}
              defaultValue={vessel?.pricePerDayMax}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  label="Największa cena dzienna [zł]"
                  fullWidth
                  {...field}
                />
              )}
            />
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="deposit"
              control={control}
              defaultValue={vessel?.deposit}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  label="Kaucja [zł]"
                  fullWidth
                  {...field}
                />
              )}
            />
          </Grid>{" "}
          <Grid item md={3} xs={0} xs={3}></Grid>
          <Grid item xs={12}>
            <Typography>Dane jachtu</Typography>
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="length"
              control={control}
              defaultValue={vessel?.length}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  multiline
                  fullWidth
                  label="Długość [m]"
                  {...field}
                />
              )}
            />
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="width"
              control={control}
              defaultValue={vessel?.width}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  multiline
                  fullWidth
                  label="Szerokość [m]"
                  {...field}
                />
              )}
            />
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="immersion"
              control={control}
              defaultValue={vessel?.immersion}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  multiline
                  fullWidth
                  label="Zanurzenie [m]"
                  {...field}
                />
              )}
            />
          </Grid>
          <Grid item md={0} xs={3}></Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="maxCrew"
              control={control}
              defaultValue={vessel?.maxCrew}
              render={({ field }) => (
                <TextField
                  variant="outlined"
                  multiline
                  fullWidth
                  label="Maks. załogi"
                  {...field}
                />
              )}
            />
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="category"
              control={control}
              defaultValue={vessel?.category}
              render={({ field }) => (
                <Select
                  variant="outlined"
                  fullWidth
                  label="Kategoria"
                  placeholder="Kategoria"
                  {...field}
                >
                  <MenuItem value={"SAILBOAT"}>Jacht żaglowy</MenuItem>
                  <MenuItem value={"MOTORBOAT"}>Jacht motorowy</MenuItem>
                </Select>
              )}
            />
          </Grid>
          <Grid item md={3} xs={12}>
            <Controller
              name="licenceNeeded"
              control={control}
              defaultValue={vessel?.licenceNeeded}
              render={({ field }) => (
                <Select
                  variant="outlined"
                  fullWidth
                  label="Patent wymagany"
                  {...field}
                >
                  <MenuItem value={true}>Patent wymagany</MenuItem>
                  <MenuItem value={false}>Patent niewymagany</MenuItem>
                </Select>
              )}
            />
          </Grid>{" "}
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleSubmit(onSubmit)}
            >
              Zapisz
            </Button>
          </Grid>
        </Grid>
      </Card>
      <Snackbar open={errorSnackbarOpen} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Nie udało się zapisać zmian. Spróbuj jeszcze raz lub skontaktuj się z
          hello@mazury.guide.
        </Alert>
      </Snackbar>
      <Snackbar open={successSnackbarOpen} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Zmiany zapisane
        </Alert>
      </Snackbar>
    </>
  );
};

export default VesselForm;
