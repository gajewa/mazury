import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useAppContext } from "../context/state";
import Layout from "./Layout";
import LocationList from "./LocationList";
import VesselList from "./VesselList";

const Map = () => {
  const [locationList, setLocationList] = useState([]);

  const router = useRouter();
  const additionalFilters = router.query;

  const emptyStringIfUndefined = (s) => (s ? s : "");

  useEffect(() => {
    axios
      .get(
        `${
          process.env.NEXT_PUBLIC_API_BASE_URL
        }/api/locations?locationType=${emptyStringIfUndefined(
          additionalFilters.type
        )}&name=${emptyStringIfUndefined(
          additionalFilters.search
        )}&vesselType=${emptyStringIfUndefined(
          additionalFilters.vesselType
        )}&maxVesselLength=${emptyStringIfUndefined(
          additionalFilters.maxVesselLength
        )}&minVesselLength=${emptyStringIfUndefined(
          additionalFilters.minVesselLength
        )}&maxVesselCrew=${emptyStringIfUndefined(
          additionalFilters.maxVesselCrew
        )}&minVesselCrew=${emptyStringIfUndefined(
          additionalFilters.minVesselCrew
        )}&licenceNeededForVessel=${emptyStringIfUndefined(
          additionalFilters.licenceNeededForVessel
        )}&vesselCategory=${emptyStringIfUndefined(
          additionalFilters.vesselCategory
        )}`
      )
      .then((res) => {
        setLocationList(res.data);
      });
  }, [additionalFilters]);

  return (
    <>
      <Layout hideOverflowY={true} locationList={locationList}>
        {additionalFilters.type === "CHARTER" ? (
          <VesselList locationList={locationList} />
        ) : (
          <LocationList locationList={locationList} />
        )}
      </Layout>
    </>
  );
};

export default Map;
