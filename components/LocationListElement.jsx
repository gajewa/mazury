import { makeStyles } from "@material-ui/core/styles";
import { useRouter } from "next/router";
import CommoditiesIcons from "./CommoditiesIcons";
import styled from "styled-components";
import { Box, Grid } from "@material-ui/core";

const LocationListElement = ({ item }) => {
  const useStyles = makeStyles((theme) => ({
    listCardRoot: {
      display: "flex",
      maxWidth: "95%",
      margin: "auto",
      marginTop: "1rem",
    },
    details: {
      display: "flex",
      flexDirection: "column",
    },
    content: {
      flex: "1 0 auto",
    },
    cover: {
      width: 250,
    },
  }));

  const classes = useStyles();
  const router = useRouter();

  const resolveIconByType = (type) => {
    if (type === "RESTAURANT") {
      return "/images/icons/002-cutlery.svg";
    } else if (type === "CHARTER") {
      return "/images/icons/map-boat.svg";
    } else if (type === "PORT") {
      return "/images/icons/map-anchor-1.svg";
    } else {
      return "/images/icons/question.svg";
    }
  };
  return (
    <StyledCard key={item.name} className={classes.listCardRoot}>
      <StyledAnchor onClick={() => router.push("/location/" + item.slug)}>
        <Box
          display="flex"
          width="100%"
          flexDirection="column"
          p=" 16px 16px 16px 24px"
          margin="0"
          justifyContent={{ xs: "center", sm: "center", lg: "flex-start" }}
        >
          <Grid container spacing={0}>
            <Grid item xs={12} md={12} lg={8}>
              <Title>
                <img src={resolveIconByType(item.type)} height="25rem" />{" "}
                {item.name}
              </Title>
            </Grid>
            <Grid item xs={12} md={12} lg={4}>
              <CommoditiesWrapper>
                <Box marginTop="0.5rem" margin="auto">
                  <CommoditiesIcons item={item} />
                </Box>
              </CommoditiesWrapper>
            </Grid>
            <Grid item xs={6} md={6} lg={8}>
              <IconsWrapper>
                <p>
                  <img src="/images/map.png" height="25rem" />{" "}
                  {`${item.locationLake} ${
                    item.locationCity ? " / " + item.locationCity : ""
                  }`}
                </p>
              </IconsWrapper>
            </Grid>
            {item.type === "PORT" && (
              <>
                <Grid item xs={6} md={6} lg={4}>
                  <IconsWrapper>
                    <p>
                      <img
                        src="/images/icons/021-swimming-pool.svg"
                        height="25rem"
                      />{" "}
                      {`${item.depthMin ? item.depthMin : "?"}m - ${
                        item.depthMax ? item.depthMax : "?"
                      }m`}
                    </p>
                  </IconsWrapper>
                </Grid>
                <Grid item xs={6} md={6} lg={8}>
                  <IconsWrapper>
                    <p>
                      <img src="/images/icons/wallet.svg" height="25rem" />{" "}
                      {`${item.pricePerVessel}zł + ${
                        item.pricePerPerson ? item.pricePerPerson : 0
                      }zł/os`}
                    </p>
                  </IconsWrapper>
                </Grid>
                <Grid item xs={6} md={6} lg={4}>
                  <IconsWrapper>
                    <p>
                      <img
                        src="/images/icons/017-anchor-1.svg"
                        height="25rem"
                      />{" "}
                      {item.stopTypes.length > 0 && item.stopTypes[0] !== ""
                        ? item.stopTypes.map((st) => st.name).join(", ")
                        : "?"}
                    </p>
                  </IconsWrapper>
                </Grid>
              </>
            )}
            {item.type === "RESTAURANT" && (
              <>
                <Grid item xs={6} md={6} lg={4}>
                  <IconsWrapper>
                    <p>
                      <img src="/images/icons/open.svg" height="25rem" />{" "}
                      {`${item.openTime ? item.openTime : "?"} - ${
                        item.closeTime ? item.closeTime : "?"
                      }`}
                    </p>
                  </IconsWrapper>
                </Grid>
              </>
            )}
          </Grid>
        </Box>
      </StyledAnchor>
    </StyledCard>
  );
};

export default LocationListElement;

const StyledCard = styled.div`
  background-color: white;
  box-shadow: 0px 8px 24px -2px rgba(0, 0, 0, 0.08);
  border-radius: 12px;
  overflow: hidden;
  transition: all 0.2s ease-in;

  :hover {
    transform: translateY(-4px);
    box-shadow: 0px 8px 24px -2px rgba(0, 0, 0, 0.16);
  }
`;
const StyledAnchor = styled.a`
  color: #7896a5 !important;
  width: 100%;
  display: flex;
  :hover {
    cursor: pointer;
  }

  @media (max-width: 624px) {
    flex-direction: column;
  }
`;

const Title = styled.h6`
  font-size: 2rem;
  color: #29343d;
  margin: 0;
  padding: 0;
`;
const CommoditiesWrapper = styled.div`
  img {
    height: 1.75rem;
  }
  margin-top: 5px;
  margin-right: 0;
  margin: auto;
`;
const IconsWrapper = styled.div`
  width: 100%;

  p {
    font-size: 1rem;
    margin: 0;
    padding: 0;
    margin-top: 1rem;
    display: flex;
    align-items: center;

    img {
      margin-right: 8px;
      height: 2 rem;
    }
  }
`;
