import {
  Box,
  Button,
  Checkbox,
  Grid,
  Input,
  Snackbar,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Controller, useForm } from "react-hook-form";
import MuiAlert from "@material-ui/lab/Alert";
import { useState } from "react";
import axios from "axios";
import { IMaskInput } from "react-imask";
import * as React from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

const GeneralInfoForm = ({ location }) => {
  const classes = useStyles();

  const { handleSubmit, control, getValues, reset, errors } = useForm();

  const onSubmit = (data) => {
    const id = location.id;
    axios
      .put(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/locations/${id}/general-info`,
        data
      )
      .then((res) => {
        setSuccessSnackbarOpen(true);
      })
      .catch((error) => {
        setErrorSnackbarOpen(true);
      });
  };

  const [successSnackbarOpen, setSuccessSnackbarOpen] = useState(false);
  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setSuccessSnackbarOpen(false);
    setErrorSnackbarOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={3} xs={12}>
          <Controller
            name="name"
            control={control}
            defaultValue={location.name}
            render={({ field }) => (
              <TextField
                fullWidth
                variant="outlined"
                label="Nazwa"
                {...field}
              />
            )}
          />
        </Grid>
        <Grid item md={3} xs={12}>
          <Controller
            name="phoneNumber"
            control={control}
            defaultValue={location.phoneNumber}
            render={({ field }) => (
              <TextField
                variant="outlined"
                fullWidth
                label="Numer telefonu"
                {...field}
              />
            )}
          />
        </Grid>
        <Grid item md={3} xs={12}>
          <Controller
            name="email"
            control={control}
            defaultValue={location.email}
            render={({ field }) => (
              <TextField
                variant="outlined"
                label="Email kontaktowy"
                fullWidth
                {...field}
              />
            )}
          />
        </Grid>
        <Grid item md={3} xs={12}>
          <Controller
            name="website"
            control={control}
            defaultValue={location.website}
            render={({ field }) => (
              <TextField
                variant="outlined"
                label="Strona www"
                fullWidth
                {...field}
              />
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <Controller
            name="description"
            control={control}
            defaultValue={location.description}
            render={({ field }) => (
              <TextField
                variant="outlined"
                multiline
                minRows={3}
                fullWidth
                label="Opis"
                {...field}
              />
            )}
          />
        </Grid>
        {location.type === "RESTAURANT" && (
          <>
            <Grid item md={3} xs={12}>
              <Controller
                name="openTime"
                control={control}
                defaultValue={location.openTime}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    label="Godzina otwarcia [HH:mm]"
                    fullWidth
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item md={3} xs={12}>
              <Controller
                name="closeTime"
                control={control}
                defaultValue={location.closeTime}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    label="Godzina zamknięcia [HH:mm]"
                    fullWidth
                    {...field}
                  />
                )}
              />
            </Grid>
          </>
        )}

        <Grid item xs={12}>
          Włączony formularz kontaktowy:
          <Controller
            name="contactFormActive"
            control={control}
            defaultValue={location.contactFormActive}
            render={({ field }) => (
              <Checkbox
                variant="outlined"
                multiline
                fullWidth
                label="Opis"
                {...field}
              />
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmit(onSubmit)}
          >
            Zapisz
          </Button>
        </Grid>
      </Grid>
      <Snackbar open={errorSnackbarOpen} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Nie udało się zapisać zmian. Spróbuj jeszcze raz lub skontaktuj się z
          hello@mazury.guide.
        </Alert>
      </Snackbar>
      <Snackbar open={successSnackbarOpen} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Zmiany zapisane
        </Alert>
      </Snackbar>
    </>
  );
};

export default GeneralInfoForm;
