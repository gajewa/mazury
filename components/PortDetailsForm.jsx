import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Controller, useForm } from "react-hook-form";
import MuiAlert from "@material-ui/lab/Alert";
import { useState } from "react";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

const PortDetailsForm = ({ location }) => {
  const classes = useStyles();

  const { handleSubmit, control, getValues, reset, errors } = useForm();

  const onSubmit = (data) => {
    const { parking, ...payload } = data;
    payload.stopTypes = Object.keys(data.parking)
      .filter((st) => data.parking[st])
      .map((st) => ({
        name: st,
      }));

    const id = location.id;
    axios
      .put(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/api/locations/${id}/port-info`,
        payload
      )
      .then((res) => {
        setSuccessSnackbarOpen(true);
      })
      .catch((error) => {
        setErrorSnackbarOpen(true);
      });
  };

  const [successSnackbarOpen, setSuccessSnackbarOpen] = useState(false);
  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);
  const [parkingOptions, setParkingOptions] = useState([
    "boja",
    "muring",
    "y bom",
    "poler",
    "dalba",
    "kotwica",
  ]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setSuccessSnackbarOpen(false);
    setErrorSnackbarOpen(false);
  };

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const renderCheckBox = (optionName) => {
    return (
      <Controller
        name={`parking.${optionName}`}
        control={control}
        defaultValue={() => {
          return !!location.stopTypes.find((a) => a.name === optionName);
        }}
        render={({ field, value }) => {
          return (
            <FormControlLabel
              control={
                <Checkbox
                  checked={field.value}
                  variant="outlined"
                  fullWidth
                  {...field}
                />
              }
              label={optionName}
            />
          );
        }}
      />
    );
  };

  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={3} xs={12}>
          <Controller
            name="minDepth"
            control={control}
            defaultValue={0.5}
            render={({ field }) => (
              <TextField
                fullWidth
                variant="outlined"
                label="Min.głębokość"
                {...field}
              />
            )}
          />
        </Grid>
        <Grid item md={3} xs={12}>
          <Controller
            name="maxDepth"
            control={control}
            defaultValue={1.5}
            render={({ field }) => (
              <TextField
                variant="outlined"
                fullWidth
                label="Max.głębokość"
                {...field}
              />
            )}
          />
        </Grid>
        <Grid item md={3} xs={12}>
          <Controller
            name="getPricePerPerson"
            control={control}
            defaultValue={1.5}
            render={({ field }) => (
              <TextField
                variant="outlined"
                fullWidth
                label="Cena za osobę"
                {...field}
              />
            )}
          />
        </Grid>
        <Grid item md={3} xs={12}>
          <Controller
            name="getPricePerVessel"
            control={control}
            defaultValue={1.5}
            render={({ field }) => (
              <TextField
                variant="outlined"
                fullWidth
                label="Cena za jednostkę"
                {...field}
              />
            )}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant="h5">Cumowanie</Typography>
        </Grid>
        <Grid item xs={12}>
          {parkingOptions.map((opt) => renderCheckBox(opt))}
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h5">Udogodnienia</Typography>
        </Grid>
        <Grid item xs={4} md={1}>
          <Controller
            name={`electricity`}
            control={control}
            defaultValue={() => {
              return location.electricity;
            }}
            render={({ field, value }) => {
              return (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={field.value}
                      variant="outlined"
                      {...field}
                    />
                  }
                  label={"Prąd"}
                />
              );
            }}
          />
        </Grid>
        <Grid item md={3} xs={8}>
          <Controller
            name="electricityPrice"
            control={control}
            defaultValue={location.electricityPrice}
            render={({ field }) => (
              <TextField variant="outlined" label="Cena [zł]" {...field} />
            )}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          <Controller
            name={`bar`}
            control={control}
            defaultValue={() => {
              return location.bar;
            }}
            render={({ field, value }) => {
              return (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={field.value}
                      variant="outlined"
                      {...field}
                    />
                  }
                  label={"Bar"}
                />
              );
            }}
          />
        </Grid>
        <Grid item xs={8} md={3}>
          <Controller
            name="barDistance"
            control={control}
            defaultValue={location.barDistance}
            render={({ field }) => (
              <TextField variant="outlined" label="Odległość [m]" {...field} />
            )}
          />
        </Grid>
        <Grid item md={1} xs={4}>
          <Controller
            name={`shower`}
            control={control}
            defaultValue={() => {
              return location.shower;
            }}
            render={({ field, value }) => {
              return (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={field.value}
                      variant="outlined"
                      {...field}
                    />
                  }
                  label={"Prysznic"}
                />
              );
            }}
          />
        </Grid>
        <Grid item md={3} xs={8}>
          <Controller
            name="showerPrice"
            control={control}
            defaultValue={location.showerPrice}
            render={({ field }) => (
              <TextField variant="outlined" label="Cena [zł]" {...field} />
            )}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          <Controller
            name={`shop`}
            control={control}
            defaultValue={() => {
              return location.shop;
            }}
            render={({ field, value }) => {
              return (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={field.value}
                      variant="outlined"
                      {...field}
                    />
                  }
                  label={"Sklep"}
                />
              );
            }}
          />
        </Grid>
        <Grid item md={3} xs={8}>
          <Controller
            name="shopDistance"
            control={control}
            defaultValue={location.shopDistance}
            render={({ field }) => (
              <TextField variant="outlined" label="Odległość [zł]" {...field} />
            )}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          <Controller
            name={`toilet`}
            control={control}
            defaultValue={() => {
              return location.toilet;
            }}
            render={({ field, value }) => {
              return (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={field.value}
                      variant="outlined"
                      {...field}
                    />
                  }
                  label={"Toaleta"}
                />
              );
            }}
          />
        </Grid>
        <Grid item md={3} xs={8}>
          <Controller
            name="toiletPrice"
            control={control}
            defaultValue={location.toiletPrice}
            render={({ field }) => (
              <TextField variant="outlined" label="Cena [zł]" {...field} />
            )}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          <Controller
            name={`water`}
            control={control}
            defaultValue={() => {
              return location.water;
            }}
            render={({ field, value }) => {
              return (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={field.value}
                      variant="outlined"
                      {...field}
                    />
                  }
                  label={"Woda"}
                />
              );
            }}
          />
        </Grid>
        <Grid item md={3} xs={8}>
          <Controller
            name="waterPrice"
            control={control}
            defaultValue={location.waterPrice}
            render={({ field }) => (
              <TextField variant="outlined" label="Cena [zł]" {...field} />
            )}
          />
        </Grid>

        <Grid item xs={12}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmit(onSubmit)}
          >
            Zapisz
          </Button>
        </Grid>
      </Grid>
      <Snackbar open={errorSnackbarOpen} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Nie udało się zapisać zmian. Spróbuj jeszcze raz lub skontaktuj się z
          hello@mazury.guide.
        </Alert>
      </Snackbar>
      <Snackbar open={successSnackbarOpen} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Zmiany zapisane
        </Alert>
      </Snackbar>
    </>
  );
};

export default PortDetailsForm;
