import {
  Box,
  Button,
  Grid,
  Hidden,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import SendIcon from "@material-ui/icons/Send";
import Link from "next/link";
import React, { useState } from "react";
import styled from "styled-components";
import { useAppContext } from "../context/state";
import LoginComponent from "./LoginComponent";
import MobileDrawer from "./MobileDrawer";

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    "&:focus": {
      backgroundColor: theme.palette.primary.main,
      "& .MuiListItemIcon-root, & .MuiListItemText-primary": {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

const LayoutWithoutMap = ({ children }) => {
  const { userData } = useAppContext();

  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Grid container spacing={0}>
        <Grid item xs={2} lg={1}>
          <Hidden mdDown>
            <LogoWrapper>
              <Link href="/">
                <a>
                  <img src="/images/logo.png" />
                </a>
              </Link>
            </LogoWrapper>
          </Hidden>
          <Hidden lgUp>
            <LogoWrapper>
              <Link href="/">
                <a>
                  <img src="/favicon.ico" />
                </a>
              </Link>
            </LogoWrapper>
          </Hidden>
        </Grid>
        <Grid item xs={8} lg={4}></Grid>
        <Hidden mdDown>
          <Grid item xs={5}></Grid>
          <Grid
            item
            xs={2}
            align="right"
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            {userData !== undefined ? (
              <div>
                <Box marginTop={"1rem"}>
                  <Button
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    variant="contained"
                    color="primary"
                    onClick={handleClick}
                  >
                    Witaj {userData?.name?.split(" ")[0]}{" "}
                    {userData.admin && "▼"}
                  </Button>
                  {userData && userData.admin && (
                    <StyledMenu
                      id="customized-menu"
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleClose}
                    >
                      <StyledMenuItem>
                        <Link href="/my-locations">
                          <a>
                            <ListItemIcon>
                              <SendIcon fontSize="small" />
                            </ListItemIcon>
                            <ListItemText primary="Moje lokalizacje" />
                          </a>
                        </Link>
                      </StyledMenuItem>
                    </StyledMenu>
                  )}
                </Box>
              </div>
            ) : (
              <Box marginTop={"1rem"}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleClick}
                >
                  Zaloguj ▼
                </Button>
                <StyledMenu
                  id="customized-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
                  <Box width="30vw">
                    <LoginComponent onLogin={handleClose} />
                  </Box>
                </StyledMenu>
              </Box>
            )}
            <Box
              marginTop={"1.5rem"}
              marginX={"0.5rem"}
              style={{
                opacity: "0.5",
              }}
            >
              <Link href="/terms">
                <a>
                  <HelpOutlineIcon />
                </a>
              </Link>
            </Box>
          </Grid>
        </Hidden>
        <MobileDrawer />
        <Grid item xs={12}>
          <DetailsWrapper>{children}</DetailsWrapper>
        </Grid>
      </Grid>
    </>
  );
};

export default LayoutWithoutMap;

const LogoWrapper = styled.div`
  margin: 1rem;
  img {
    height: 5vh;
  }
`;

const DetailsWrapper = styled.div`
  background-color: #f0f0f0 !important;
  overflow-y: scroll;
  overflow-x: hidden;
  height: 90vh;
  width: 100%;
`;
