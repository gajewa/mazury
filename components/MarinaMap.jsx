import "leaflet-defaulticon-compatibility";
import "leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css";
import "leaflet/dist/leaflet.css";
import { ImageOverlay, MapConsumer, MapContainer } from "react-leaflet";

const MarinaMap = ({ data }) => {
  const bottomLeftCorner = L.latLng(-50, -50);
  const topRightCorner = L.latLng(100, 250);

  const bounds = L.latLngBounds(bottomLeftCorner, topRightCorner);
  return (
    <MapContainer
      center={[50, 85]}
      zoom={3}
      scrollWheelZoom={true}
      style={{ height: "90vh", width: "50vw" }}
    >
      <MapConsumer>
        {(map) => {
          map.setView(L.latLng(50, 85), 2);
          return null;
        }}
      </MapConsumer>
      <ImageOverlay url="/images/svg.svg" bounds={bounds} />
    </MapContainer>
  );
};

export default MarinaMap;
