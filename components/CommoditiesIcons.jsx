const CommoditiesIcons = ({ item }) => {
  return (
    <>
      {item.bar ? <img src={"/images/icons/002-cutlery.svg"} /> : <></>}
      {item.electricity ? (
        <img src={"/images/icons/004-lightning.svg"} />
      ) : (
        <></>
      )}
      {item.shower ? <img src={"/images/icons/006-shower-1.svg"} /> : <></>}
      {item.shop ? (
        <img src={"/images/icons/009-shopping-cart-2.svg"} />
      ) : (
        <></>
      )}
      {item.toilet ? <img src={"/images/icons/013-toilet-2.svg"} /> : <></>}
      {item.water ? <img src={"/images/icons/015-drop-1.svg"} /> : <></>}
    </>
  );
};

export default CommoditiesIcons;
