import { makeStyles } from "@material-ui/core/styles";
import { useRouter } from "next/router";
import CommoditiesIcons from "./CommoditiesIcons";
import styled from "styled-components";
import { Box, Grid, Typography } from "@material-ui/core";

const VesselListElement = ({ item }) => {
  const useStyles = makeStyles((theme) => ({
    listCardRoot: {
      display: "flex",
      maxWidth: "95%",
      margin: "auto",
      marginTop: "1rem",
    },
    details: {
      display: "flex",
      flexDirection: "column",
    },
    content: {
      flex: "1 0 auto",
    },
    cover: {
      width: 250,
    },
  }));

  const classes = useStyles();

  const router = useRouter();

  const renderPrices = (ves) => {
    if (ves.pricePerDayMax && ves.pricePerDayMin) {
      if (ves.pricePerDayMax === ves.pricePerDayMin) {
        return `Cena: ${ves.pricePerDayMax}zł / doba`;
      }

      return `Cena: ${ves.pricePerDayMin}zł - ${ves.pricePerDayMax}zł / doba`;
    }

    if (ves.pricePerDayMin) {
      return `Cena: od ${ves.pricePerDayMin} zł / doba`;
    }

    if (ves.pricePerDayMax) {
      return `Cena: do ${ves.pricePerDayMax} zł / doba`;
    }

    return "";
  };

  return (
    <StyledCard key={item.type} className={classes.listCardRoot}>
      <StyledAnchor
        onClick={() => router.push(`/location/${item.location.slug}`)}
      >
        <StyledImg
          src={
            item.category === "SAILBOAT"
              ? "/images/sailing-yacht.jpg"
              : "/images/speedboat.jpg"
          }
        />
        <InformationWrapper>
          <Grid container>
            <Grid item xs={12}>
              <Title>{item.type}</Title>
            </Grid>

            {item.name && (
              <Grid item xs={12}>
                <VesselNameSubtitle>{item.name}</VesselNameSubtitle>
              </Grid>
            )}
            <Grid item xs={12}>
              <VesselNameSubtitle>{item.location.name}</VesselNameSubtitle>
            </Grid>
            <Grid item xs={12}>
              <Typography>{renderPrices(item)}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>Długość: {item.length}m</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>Szerokość: {item.width}m</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>Zanurzenie: {item.immersion}m</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>Załoga: {item.maxCrew} os.</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>
                Patent {item.licenceNeeded ? "" : "nie"}wymagany
              </Typography>
            </Grid>
          </Grid>
        </InformationWrapper>
      </StyledAnchor>
    </StyledCard>
  );
};

export default VesselListElement;

const StyledImg = styled.img`
  overflow: hidden;
  min-width: 160px;
  height: 100%;
  width: 100%;
  object-fit: cover;
`;

const StyledCard = styled.div`
  background-color: white;
  box-shadow: 0px 8px 24px -2px rgba(0, 0, 0, 0.08);
  border-radius: 12px;
  overflow: hidden;
  transition: all 0.2s ease-in;

  :hover {
    transform: translateY(-4px);
    box-shadow: 0px 8px 24px -2px rgba(0, 0, 0, 0.16);
  }
`;

const StyledAnchor = styled.a`
  color: #7896a5 !important;
  width: 100%;
  display: flex;
  :hover {
    cursor: pointer;
  }

  @media (max-width: 624px) {
    flex-direction: column;
  }
`;

const Title = styled.h6`
  font-size: 2rem;
  color: #29343d;
  margin: 0;
  padding: 0;
`;

const VesselNameSubtitle = styled.h3`
  color: #29343d;
  padding: 0px;
  margin-top: 0rem;
  margin-bottom: 1rem;
`;

const InformationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px 16px 16px 24px;
  margin: 0px;
  width: 100%;
`;
