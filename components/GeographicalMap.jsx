import { Box, Grid } from "@material-ui/core";
import "leaflet-defaulticon-compatibility";
import "leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css";
import "leaflet/dist/leaflet.css";
import { useRouter } from "next/router";
import {
  MapConsumer,
  MapContainer,
  Marker,
  Popup,
  TileLayer,
  Tooltip,
  ZoomControl,
} from "react-leaflet";

const GeographicalMap = ({
  data,
  startLat = 54.03574912643642,
  startLen = 21.765518560894712,
  startZoom = 7,
  reduecedMap,
}) => {
  const router = useRouter();
  return (
    <>
      <MapContainer
        center={[startLat, startLen]}
        zoom={startZoom}
        scrollWheelZoom={true}
        style={{
          height: reduecedMap ? "40vh" : "90vh",
          width: "100%",
        }}
        zoomControl={false}
      >
        <MapConsumer>
          {(map) => {
            map.setView(L.latLng(startLat, startLen), startZoom);
            return null;
          }}
        </MapConsumer>
        <TileLayer
          url={`https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibWF0ZXVzendndCIsImEiOiJja3M1dTM1MHgwNGhxMnhxeWtwaGlrd244In0.MjBIUItR0pe7rb3n83sRew`}
          attribution='Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery &copy; <a href="https://www.mapbox.com/">Mapbox</a>'
        />
        <ZoomControl position="bottomleft" />

        {data?.length > 0 &&
          data.map((item) => {
            var portIcon = L.icon({
              iconUrl:
                item.type === "PORT"
                  ? "/images/icons/map-anchor-1.svg"
                  : item.type === "CHARTER"
                  ? "/images/icons/map-boat.svg"
                  : item.type === "RESTAURANT"
                  ? "/images/icons/002-cutlery.svg"
                  : "/images/icons/question.svg",
              iconSize: [30, 40],
            });

            return (
              <Marker
                key={item.name}
                position={[item.lat, item.lang]}
                icon={portIcon}
                draggable={false}
                animate={true}
                eventHandlers={{
                  click: (e) => {
                    const a = data.find(
                      (a) => a.lat == e.latlng.lat && a.lang == e.latlng.lng
                    );
                    router.push("/location/" + a.slug);
                  },
                }}
              >
                <Tooltip direction="top" offset={[0, -10]}>
                  <b>{item.name}</b>
                </Tooltip>
              </Marker>
            );
          })}
      </MapContainer>
    </>
  );
};

export default GeographicalMap;
