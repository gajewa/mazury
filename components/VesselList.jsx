import { useCallback, useEffect, useRef, useState } from "react";
import { useAppContext } from "../context/state";
import YachtListElement from "./LocationListElement";
import { VariableSizeList as List } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import { useWindowResize } from "../lib/ga/useWindowResize";
import axios from "axios";
import VesselListElement from "./VesselListElement";

const VesselList = ({ locationList }) => {
  const Row = ({ data, index, setSize, windowWidth }) => {
    const rowRef = useRef();

    useEffect(() => {
      setSize(index, rowRef.current.getBoundingClientRect().height + 20);
    }, [setSize, index, windowWidth]);

    return <div ref={rowRef}>{<VesselListElement item={data[index]} />}</div>;
  };

  const listRef = useRef();
  const sizeMap = useRef({});
  const setSize = useCallback((index, size) => {
    sizeMap.current = { ...sizeMap.current, [index]: size };
    listRef.current.resetAfterIndex(index);
  }, []);
  const getSize = (index) => sizeMap.current[index] || 250;
  const [windowWidth] = useWindowResize();
  const yachts = locationList.map((l) => l.vessels).flat();

  return (
    <>
      <AutoSizer>
        {({ height, width }) => (
          <List
            ref={listRef}
            className="List"
            height={height}
            itemCount={yachts.length}
            itemSize={getSize}
            width={width}
            itemData={yachts}
          >
            {({ data, index, style }) => (
              <div style={style}>
                <Row
                  data={data}
                  index={index}
                  setSize={setSize}
                  windowWidth={windowWidth}
                />
              </div>
            )}
          </List>
        )}
      </AutoSizer>
    </>
  );
};

export default VesselList;
