import {
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Grid,
  TextField,
} from "@material-ui/core";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import Select from "react-select";
import { toast, ToastContainer } from "react-nextjs-toast";

const ReservationForm = ({ shownItem, setReservationFormVisibility }) => {
  const { control } = useForm();
  const [vesselFee, setVesselFee] = useState(0);
  const [includeElectricityFee, setIncludeElectricityFee] = useState(false);
  const [includeWaterFee, setIncludeWaterFee] = useState(false);
  const [loading, setLoading] = useState(false);
  const calculateFee = () => {
    let fee = 0;
    fee = fee + vesselFee;
    if (includeElectricityFee) {
      fee = fee + 10;
    }
    if (includeWaterFee) {
      fee = fee + 15;
    }
    return fee;
  };

  return (
    <>
      <ToastContainer />
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Controller
            control={control}
            name="vesselName"
            rules={{ required: true }}
            render={({ field }) => (
              <TextField {...field} label="Nazwa jednsotki" fullWidth={true} />
            )}
          />
        </Grid>
        <Grid item xs={6}>
          <Controller
            render={({ field }) => (
              <Select
                {...field}
                options={[
                  { value: 80, label: "do 9 m / 80 zł" },
                  { value: 90, label: "powyżej 9m / 90 zł" },
                ]}
                label={"Długość jednostki"}
                onChange={(e) => {
                  setVesselFee(e.value);
                }}
              />
            )}
            name="vesselLength"
            control={control}
            styles={{
              menuPortal: (base) => ({ ...base, zIndex: 9999 }),
            }}
            valueName="value"
            rules={{ required: true }}
          />
        </Grid>
        <Grid item xs={12}>
          <Controller
            control={control}
            name="name"
            rules={{ required: true }}
            render={({ field }) => (
              <TextField {...field} label="Imię i nazwisko" fullWidth={true} />
            )}
          />
        </Grid>
        <Grid item xs={6}>
          <Controller
            control={control}
            name="email"
            rules={{ required: true }}
            render={({ field }) => (
              <TextField {...field} label="Email" fullWidth={true} />
            )}
          />
        </Grid>
        <Grid item xs={6}>
          <Controller
            control={control}
            name="phoneNumber"
            rules={{ required: true }}
            render={({ field }) => (
              <TextField {...field} label="Numer telefonu" fullWidth={true} />
            )}
          />
        </Grid>
        <Grid item xs={6}>
          <FormControlLabel
            control={
              <Controller
                name="electricity"
                control={control}
                render={(props) => (
                  <Checkbox
                    {...props}
                    label="Electricity"
                    checked={includeElectricityFee}
                    onChange={(e) =>
                      setIncludeElectricityFee(!includeElectricityFee)
                    }
                  />
                )}
              />
            }
            label="Electricity (+ 10 zł)"
          />
        </Grid>
        <Grid item xs={4}>
          <FormControlLabel
            control={
              <Controller
                name="water"
                control={control}
                render={(props) => (
                  <Checkbox
                    {...props}
                    label="Water"
                    checked={includeWaterFee}
                    onChange={(e) => setIncludeWaterFee(!includeWaterFee)}
                  />
                )}
              />
            }
            label="Water (+ 15 zł)"
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            style={{ width: "100%" }}
            variant="contained"
            color="primary"
            onClick={() => {
              setLoading(true);
              setTimeout(() => {
                toast.notify(`Reservation complete, confirmation email sent.`);
                setReservationFormVisibility(false);
              }, 3000);
            }}
          >
            Zapłać {calculateFee()}zł{" "}
            {loading ? (
              <CircularProgress color={"white"} size={"2rem"} />
            ) : (
              <></>
            )}
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default ReservationForm;
