import { Grid } from "@material-ui/core";

const CommodityIconWithDescription = ({
  isCommodityAvailable,
  iconName,
  descriptionIfAvailable,
  small,
}) => {
  return (
    <Grid container alignItems="center" justify="center">
      <div
        style={{
          opacity: isCommodityAvailable ? "1" : "0.25",
        }}
      >
        <Grid item xs={12} align="center">
          <img
            height={small ? "35rem" : "50rem"}
            src={`/images/icons/${iconName}`}
          />
        </Grid>
        <Grid item xs={12} align="center">
          {isCommodityAvailable ? descriptionIfAvailable : "brak"}
        </Grid>
      </div>
    </Grid>
  );
};

export default CommodityIconWithDescription;
