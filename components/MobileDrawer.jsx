import {
  Box,
  Button,
  Grid,
  Hidden,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  MenuItem,
  SwipeableDrawer,
  TextField,
  Typography,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { Controller } from "react-hook-form";
import { useAppContext } from "../context/state";
import Select from "react-select";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const MobileDrawer = ({ control, getValues, submitFilters }) => {
  const { userData, locationTypeToShow, setLocationTypeToShow } =
    useAppContext();

  const router = useRouter();

  const classes = useStyles();

  const [drawerOpen, setDrawerOpen] = useState(false);
  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setDrawerOpen(open);
  };

  const anchor = "right";

  return (
    <>
      <Hidden lgUp>
        <Grid item xs={2}>
          <React.Fragment key={anchor}>
            <Box textAlign="right" marginTop="1.5rem" marginRight="1rem">
              <MenuIcon onClick={toggleDrawer(anchor, true)} />
            </Box>

            <SwipeableDrawer
              anchor={"left"}
              open={drawerOpen}
              onClose={toggleDrawer(anchor, false)}
              onOpen={toggleDrawer(anchor, true)}
              style={{ zIndex: "99999999999" }}
            >
              <Box padding={"2rem"} width={"60vw"}>
                <Box display="flex" marginTop="0.25rem">
                  <Typography variant="h5">Filtruj:</Typography>
                </Box>
                <Box display="flex" marginTop="0.5rem">
                  <Box display="flex">
                    <img
                      src="/images/icons/all.svg"
                      height="40"
                      style={{
                        opacity: locationTypeToShow !== "" ? "0.25" : "1",
                      }}
                      onClick={() => {
                        const query = router.query;
                        delete query.type;
                        router.push({
                          path: router.path,
                          query: query,
                        });
                      }}
                    />
                  </Box>
                  <Box marginLeft={"0.5rem"} display="flex">
                    <img
                      src="/images/icons/map-anchor-1.svg"
                      height="40"
                      style={{
                        opacity:
                          locationTypeToShow !== "" &&
                          locationTypeToShow !== "PORT"
                            ? "0.25"
                            : "1",
                      }}
                      onClick={() => {
                        const query = router.query;
                        query.type = "PORT";
                        router.push({
                          path: router.path,
                          query: query,
                        });
                      }}
                    />
                  </Box>
                  <Box marginLeft={"0.5rem"} display="flex">
                    <img
                      src="/images/icons/map-boat.svg"
                      height="40"
                      style={{
                        opacity:
                          locationTypeToShow !== "" &&
                          locationTypeToShow !== "CHARTER"
                            ? "0.25"
                            : "1",
                      }}
                      onClick={() => {
                        const query = router.query;
                        query.type = "CHARTER";
                        router.push({
                          path: router.path,
                          query: query,
                        });
                      }}
                    />
                  </Box>
                  <Box marginLeft={"0.5rem"} display="flex">
                    <img
                      src="/images/icons/002-cutlery.svg"
                      height="40"
                      style={{
                        opacity:
                          locationTypeToShow !== "" &&
                          locationTypeToShow !== "RESTAURANT"
                            ? "0.25"
                            : "1",
                      }}
                      onClick={() => {
                        const query = router.query;
                        query.type = "RESTAURANT";
                        router.push({
                          path: router.path,
                          query: query,
                        });
                      }}
                    />
                  </Box>
                  <Box marginLeft={"0.5rem"} display="flex">
                    <img
                      src="/images/icons/question.svg"
                      height="40"
                      style={{
                        opacity:
                          locationTypeToShow !== "" &&
                          locationTypeToShow !== "OTHER"
                            ? "0.25"
                            : "1",
                      }}
                      onClick={() => {
                        const query = router.query;
                        query.type = "OTHER";
                        router.push({
                          path: router.path,
                          query: query,
                        });
                      }}
                    />
                  </Box>
                </Box>
                {locationTypeToShow === "CHARTER" && (
                  <Box marginLeft={"0.5rem"}>
                    <Grid container spacing={2}>
                      <Grid item xs={12}>
                        <Controller
                          name="minVesselLength"
                          control={control}
                          defaultValue={router.query.minVesselLength}
                          render={({ field }) => (
                            <TextField
                              fullWidth
                              label="Min. długość"
                              size="small"
                              {...field}
                            />
                          )}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Controller
                          name="maxVesselLength"
                          defaultValue={router.query.maxVesselLength}
                          control={control}
                          render={({ field }) => (
                            <TextField
                              fullWidth
                              label="Max. długość"
                              size="small"
                              {...field}
                            />
                          )}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Controller
                          name="minVesselCrew"
                          control={control}
                          defaultValue={router.query.minVesselCrew}
                          render={({ field }) => (
                            <TextField
                              fullWidth
                              label="Min ilość załogi"
                              size="small"
                              {...field}
                            />
                          )}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Controller
                          name="maxVesselCrew"
                          control={control}
                          defaultValue={router.query.maxVesselCrew}
                          render={({ field }) => (
                            <TextField
                              fullWidth
                              label="Max ilość załogi"
                              size="small"
                              {...field}
                            />
                          )}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Controller
                          render={({ field }) => (
                            <Select
                              {...field}
                              placeholder="Patent"
                              isClearable
                              defaultValue={[
                                { label: "Patent wymagany", value: "true" },
                                {
                                  label: "Patent niewymagany",
                                  value: "false",
                                },
                              ].find(
                                (el) =>
                                  el.value ===
                                  router.query.licenceNeededForVessel
                              )}
                              options={[
                                { label: "Patent wymagany", value: "true" },
                                {
                                  label: "Patent niewymagany",
                                  value: "false",
                                },
                              ]}
                            />
                          )}
                          name="licenceNeededForVessel"
                          control={control}
                          rules={{ required: true }}
                          styles={{
                            menuPortal: (base) => ({
                              ...base,
                              zIndex: 9999,
                            }),
                          }}
                          menuPortalTarget={document.body}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Controller
                          render={({ field }) => (
                            <Select
                              {...field}
                              isClearable
                              placeholder="Typ jachtu"
                              defaultValue={[
                                { label: "Żaglowy", value: "SAILBOAT" },
                                {
                                  label: "Motorowy / Houseboat",
                                  value: "MOTORBOAT",
                                },
                              ].find(
                                (el) => el.value === router.query.vesselCategory
                              )}
                              options={[
                                { label: "Żaglowy", value: "SAILBOAT" },
                                {
                                  label: "Motorowy / Houseboat",
                                  value: "MOTORBOAT",
                                },
                              ]}
                            />
                          )}
                          name="vesselCategory"
                          control={control}
                          rules={{ required: true }}
                          styles={{
                            menuPortal: (base) => ({
                              ...base,
                              zIndex: 9999,
                            }),
                          }}
                          menuPortalTarget={document.body}
                        />
                      </Grid>
                      <Grid item xs={6}></Grid>
                      <Grid item xs={6}>
                        <Button
                          onClick={() => {
                            submitFilters();
                            setDrawerOpen(false);
                          }}
                          fullWidth
                          variant="contained"
                          color="primary"
                        >
                          Szukaj
                        </Button>
                      </Grid>
                    </Grid>
                  </Box>
                )}
                <Box
                  mt={3}
                  position="absolute"
                  bottom="5px"
                  style={{
                    opacity: "0.5",
                  }}
                >
                  <List
                    component="nav"
                    aria-labelledby="nested-list-subheader"
                    className={classes.root}
                  >
                    {userData && userData.admin && (
                      <ListItem
                        button
                        onClick={() => {
                          router.push("/my-locations");
                        }}
                      >
                        <ListItemText
                          primaryTypographyProps={{
                            style: { fontWeight: "bold" },
                          }}
                          primary="Moje lokalizacje"
                        />
                      </ListItem>
                    )}
                    {!userData && (
                      <ListItem
                        button
                        onClick={() => {
                          router.push("/login");
                        }}
                      >
                        <ListItemText
                          primaryTypographyProps={{
                            style: { fontWeight: "bold" },
                          }}
                          primary="Zaloguj"
                        />
                      </ListItem>
                    )}
                    <ListItem
                      button
                      onClick={() => {
                        router.push("/terms");
                      }}
                    >
                      <ListItemText
                        primaryTypographyProps={{
                          style: { fontWeight: "bold" },
                        }}
                        primary="Polityka prywatności"
                      />
                    </ListItem>
                  </List>
                </Box>
              </Box>
            </SwipeableDrawer>
          </React.Fragment>
        </Grid>
      </Hidden>
    </>
  );
};

export default MobileDrawer;
