import axios from "axios";
import { createContext, useContext, useEffect, useState } from "react";
import cookie from "cookie-cutter";

const AppContext = createContext();

export function AppWrapper({ children }) {
  const [responseData, setResponseData] = useState();
  const [token, setToken] = useState();
  //cookie.get("mazuryGuideToken")

  useEffect(() => {
    const config = {};
    if (token) {
      config.headers = { Authorization: token };
    }

    axios
      .get(`${process.env.NEXT_PUBLIC_API_BASE_URL}/api/users/current`, config)
      .then((res) => {
        setResponseData(res.data);
      })
      .catch((error) => {
        setResponseData(undefined);
      });
  }, [token]);

  const [locationTypeToShow, setLocationTypeToShow] = useState("");
  const [locationNameQuery, setLocationNameQuery] = useState("");
  const userData = responseData ? responseData : undefined;

  let sharedState = {
    userData,
    locationTypeToShow,
    setLocationTypeToShow,
    locationNameQuery,
    setLocationNameQuery,
    setToken,
    token,
  };

  return (
    <AppContext.Provider value={sharedState}>{children}</AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}
